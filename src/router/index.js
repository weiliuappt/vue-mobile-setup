import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'

Vue.use(Router)
// 需要左方向动画的路由用this.$router.to('****')
Router.prototype.togo = function (path) {
  this.isleft = true
  this.isright = false
  this.push(path)
}
// 需要右方向动画的路由用this.$router.goRight('****')
Router.prototype.goRight = function (path) {
  this.isright = true
  this.isleft = false
  this.push(path)
}
// 需要返回按钮动画的路由用this.$router.goBack()，返回上一个路由
Router.prototype.goBack = function () {
  this.isright = true
  this.isleft = false
  this.go(-1)
}
// 点击浏览器返回按钮执行，此时不需要路由回退
Router.prototype.togoback = function () {
  this.isright = true
  this.isleft = false
}
// 点击浏览器前进按钮执行
Router.prototype.togoin = function () {
  this.isright = false
  this.isleft = true
}

var vueRouter = new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: (resolve) => require(['@/pages/index'], resolve),
      redirect: '/home',
      children: [
        {
          path: '/home',
          name: 'home',
          component: (resolve) => require(['@/pages/index/home'], resolve)
        },
        {
          path: '/story',
          name: 'story',
          component: (resolve) => require(['@/pages/index/story'], resolve)
        },
        {
          path: '/footprints',
          name: 'footprints',
          component: (resolve) => require(['@/pages/index/footprints'], resolve)
        },
        {
          path: '/day',
          name: 'day',
          component: (resolve) => require(['@/pages/index/day'], resolve)
        }
      ]
    },
    {
      path: '/Home/Detail',
      name: 'Detail',
      component: (resolve) => require(['@/pages/detail'], resolve),
      meta: {auth: true}
    },
    {
      path: '/login',
      name: 'login',
      component: (resolve) => require(['@/pages/login'], resolve)
    },
    {
      path: '/subscribe',
      name: 'subscribe',
      component: (resolve) => require(['@/pages/subscribe'], resolve)
    }
  ]
})

vueRouter.beforeEach((to, from, next) => {
  if (to.matched.some(m => m.meta.auth)) {
    if (store.getters.isLogin === '100') {
      next()
    } else {
      next({path: '/login', query: {redirect: to.fullPath}})
    }
  } else {
    next()
  }
})

export default vueRouter

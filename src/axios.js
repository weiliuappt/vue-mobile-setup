import axios from 'axios'
// import store from './store'
// import router from './router'

var instance = axios.create({
  timeout: 5000,
  headers: {'Contet-type': 'application/json;charset=UTF-8'},
})

instance.interceptors.request.use(
  config => {
    return config
  }
)

instance.interceptors.response.use(
  response => {
    return response
  }
)

export default {
  userLogin(data) {
    return instance.get('/api/login', data);
  }
}
